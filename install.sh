#!/bash/bin

# Variable inits
CODE_DOWNLOAD_URL="https://bitbucket.org/rvosocietyweather/raspberry-pi-python-code/get/master.zip"
DHT_DOWNLOAD_URL="https://github.com/adafruit/Adafruit_Python_DHT/archive/master.zip"

# Update raspberry pi
(sudo apt-get update -y)
(sudo apt-get install build-essential python-dev unzip -y)

echo $TMP_FOLDER "/code"

# Checks if script is run as root
if [ "$EUID" -ne 0 ]; then
    echo "Je moet dit bestand als root uitvoeren."
    echo "sudo bash install.sh"
    exit
fi

# Creates a new directory in the temp folder if it doesn't exist yet
if [ ! -d "/tmp/bosonline" ]; then
    mkdir "/tmp/bosonline"
fi

# Downloads the python code
(wget $CODE_DOWNLOAD_URL -O "/tmp/bosonline/code.zip" -q --show-progress)

# Ask which sensors the user wants to install
SENSORS=$(whiptail --title "Selecteer de sensors die je wil gebruiken" --checklist \
"Kies je sensoren" 15 60 4 \
"RAIN_1" "Regen sensor 1" ON \
"RAIN_2" "Regen sensor 2" ON \
"HPMA115S0" "Fijnstof meter PM2.5 en PM10" OFF \
"DHT22" "Luchtdruk, luchtvochtigheid en temp" OFF 3>&1 1>&2 2>&3)

exitstatus=$?

if [ $exitstatus = 0 ]; then
    for choice in $SENSORS
    do
        echo $choice
    done
else
    echo "Geannuleerd"
    exit
fi
